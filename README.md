# World Happiness 2022

As I find [the World Happiness Research](https://worldhappiness.report/ed/2022/) very interesting, I wanted to have a look on the data published for your 2022. 

In the end it led me to two projects:
* In [the first one](https://www.ozgurakyazi.com/projects/world-happiness-report-2022?mtm_campaign=gitlab), the same model as in original report has been used but using the data, which was imputed without detailed research for each row and column. It is interesting that the results are on par with the original report.
* [The second one](https://www.ozgurakyazi.com/projects/machine-learning-and-world-happiness-report-2022?mtm_campaign=gitlab) started with a question of why not pushing the performance of with machine learning. Carefully avoiding any data leakage, the results were significantly better than the original report, magic of XGBoost model. Also the magic has also been uncovered with SHAP values.

For thoses who are interested in the code, this repository should be self-explanatory with all the comments.
