"""
    Original version:
        https://stackoverflow.com/questions/35611465/python-scikit-learn-clustering-with-missing-data
"""

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans

def kmeans_nan(X, n_clusters, random_state, max_iter=10):
    """Perform K-Means clustering on data with missing values.

    Args:
      X: An [n_samples, n_features] array of data to cluster.
      n_clusters: Number of clusters to form.
      max_iter: Maximum number of EM iterations to perform.

    Returns:
      cls: The trained sklearn.cluster.KMeans object
      X_hat: The input data, imputed with the final centroids
    """

    # Initialize missing values to their column means
    missing = pd.isna(X)
    mu = np.nanmean(X, 0, keepdims=1)
    X_hat = np.where(missing, mu, X)

    for i in range(max_iter):
        if i > 0:
            # initialize KMeans with the previous set of centroids. this is much
            # faster and makes it easier to check convergence (since labels
            # won't be permuted on every iteration), but might be more prone to
            # getting stuck in local minima.
            cls = KMeans(n_clusters, init=prev_centroids, n_init=1, random_state=random_state)
        else:
            # do multiple random initializations in parallel
            cls = KMeans(n_clusters, random_state=random_state)

        # perform clustering on the filled-in data
        labels = cls.fit_predict(X_hat)
        centroids = cls.cluster_centers_

        # fill in the missing values based on their cluster centroids
        X_hat[missing] = centroids[labels][missing]

        # when the labels have stopped changing then we have converged
        if i > 0 and np.all(labels == prev_labels):
            break

        prev_labels = labels
        prev_centroids = cls.cluster_centers_

    return cls, X_hat
